import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchMessages, fetchNewMessage, postMessageReq} from "../store/actions";
import NewMessage from "../components/NewMessage/NewMessage";
import FileInput from "../components/FileInput/FileInput";


const Messages = () => {
    const [state, setState] = useState({
        author: "",
        message: "",
        image: ""
    });

    const dispatch = useDispatch();
    const messages = useSelector(state => state.messages);
    const error = useSelector(state => state.error);

    useEffect(() => {
        dispatch(fetchMessages());
    }, [dispatch]);


    // useEffect(() => {
    //     const interval = setInterval(() => {
    //        dispatch(fetchNewMessage(messages))
    //     }, 5000)
    //     return () => clearInterval(interval);
    // }, [dispatch, messages]);

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };



    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const onSubmitHandler = e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        // onSubmit(formData);
        dispatch(postMessageReq(formData))
        console.log(state);
    };

    // const fileChangeHandler = e => {
    //     const name = e.target.name;
    //     const file = e.target.files[0];
    //     setState(prevState => ({...prevState, [name]: file}));
    // };

    // const submitFormHandler = e => {
    //     e.preventDefault();
    //     const formData = new FormData();
    //     Object.keys(state).forEach(key => {
    //         formData.append(key, state[key]);
    //     });
    //     onSubmit(formData);
    // };

    // const onClickHandler = () => {
    //     const obj = {
    //         message: state.message,
    //         author: state.author
    //     }
    //     dispatch(postMessageReq(obj))
    // };

    let errorMessage = '';
    if (error) {
        errorMessage = <h1>{error}</h1>
        return errorMessage;
    }

 // let mesArray = messages.map(mes => {
 //        return <NewMessage
 //            key={mes.id}
 //            id={mes.id}
 //            author={mes.author}
 //            date={mes.date}
 //            image={mes.image}
 //            message={mes.message}/>;
 //    });

    return (
    <div className="container bg-secondary">
        {errorMessage}
        <form className="p-5" onSubmit={onSubmitHandler}>
            <div className="input-group mb-3">
                <div className="input-group-prepend">
                    <span className="input-group-text" id="basic-addon1">@</span>
                </div>
                <input type="text" className="form-control" id="author" placeholder="Username" aria-label="Username"
                       aria-describedby="basic-addon1" onChange={inputChangeHandler} name="author"/>
            </div>
            <FileInput
                name="image"
                onChange={fileChangeHandler}
            />

            <div className="input-group mb-3">
                <div className="input-group-prepend">
                    <span className="input-group-text">With textarea</span>
                </div>
                <textarea className="form-control" aria-label="With textarea" id="message"
                          placeholder="Message" onChange={inputChangeHandler} name="message"/>
            </div>
            <button type="submit" className="btn btn-info" id="send-btn" >Send message</button>
        </form>
        {messages.map(mes => {
            return <NewMessage
                key={mes.id}
                id={mes.id}
                author={mes.author}
                date={mes.date}
                image={mes.image}
                message={mes.message}/>;
        })}
    </div>
    );
};

export default Messages;