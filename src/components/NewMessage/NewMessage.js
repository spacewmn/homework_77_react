import React from 'react';
import {apiURL} from "../../constants";

const NewMessage = (props) => {
    let cardImage;
    if (props.image) {
        cardImage = apiURL + '/uploads/' + props.image;
    } else {
        cardImage = null
    }


    return (
        <div className="card mb-3" id={props.id}>
                <div className="toast-header">
                    <strong className="mr-auto">{props.author}</strong>
                    <small className="text-muted">{props.date}</small>
                </div>
                <div className="card-body">
                    <img src={cardImage} className="card-img-top" alt="..."/>
                    <p className="card-text">{props.message}</p>
                </div>
        </div>

        // <div aria-live="assertive" aria-atomic="true" className="alert alert-secondary" role="alert" id={props.id}>
        //     <div className="toast-header">
        //         <strong className="mr-auto">{props.author}</strong>
        //         <small className="text-muted">{props.date}</small>
        //     </div>
        //     <div className="toast-body">
        //         {props.message}
        //     </div>
        // </div>
    );
};

export default NewMessage;