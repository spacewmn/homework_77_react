import React from 'react';

const FileInput = (props) => {
    return (
        // <div className="input-group mb-3">
        //     <div className="custom-file mb-3">
        //         <input
        //             type="file"
        //             className="custom-file-input"
        //             id="inputGroupFile01"
        //             name={props.name}
        //             onChange={props.onChange}
        //         />
        //             <label className="custom-file-label" htmlFor="inputGroupFile01">Choose file</label>
        //     </div>
        //     <div className="input-group-append">
        //         <span className="input-group-text" id="inputGroupFileAddon02">Upload</span>
        //     </div>
        // </div>

        <div className="input-group mb-3">
            <div className="input-group-prepend">
                <span className="input-group-text" id="inputGroupFileAddon01">Upload</span>
            </div>
            <div className="custom-file">
                <input type="file"
                       className="custom-file-input"
                       id="inputGroupFile01"
                       aria-describedby="inputGroupFileAddon01"
                       name={props.name}
                       onChange={props.onChange}
                />
                    <label className="custom-file-label" htmlFor="inputGroupFile01">Choose file</label>
            </div>
        </div>


);
};

export default FileInput;